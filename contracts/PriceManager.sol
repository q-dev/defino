// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.17;

import "@solarity/solidity-lib/contracts-registry/AbstractDependant.sol";

import "./interfaces/IRegistry.sol";
import "./interfaces/IPriceManager.sol";
import "./interfaces/ISystemPoolsRegistry.sol";

import "./common/Globals.sol";

contract PriceManager is IPriceManager, AbstractDependant {
    ISystemPoolsRegistry internal _systemPoolsRegistry;

    mapping(bytes32 => PriceFeed) public priceFeeds;

    function setDependencies(address contractsRegistry_, bytes memory) public override dependant {
        _systemPoolsRegistry = ISystemPoolsRegistry(
            IRegistry(contractsRegistry_).getSystemPoolsRegistryContract()
        );
    }

    function addOracle(
        bytes32 assetKey_,
        address assetAddr_,
        address qPriceFeed_
    ) external override {
        require(
            address(_systemPoolsRegistry) == msg.sender,
            "PriceManager: Caller not a SystemPoolsRegistry."
        );

        (, ISystemPoolsRegistry.PoolType poolType_) = _systemPoolsRegistry.poolsInfo(assetKey_);

        if (poolType_ == ISystemPoolsRegistry.PoolType.LIQUIDITY_POOL) {
            require(
                qPriceFeed_ != address(0),
                "PriceManager: The oracle must not be a null address."
            );
        }

        priceFeeds[assetKey_] = PriceFeed(assetAddr_, IFxPriceFeed(qPriceFeed_));

        emit OracleAdded(assetKey_, qPriceFeed_);
    }

    function getPrice(bytes32 assetKey_) external view override returns (uint256, uint8) {
        require(
            priceFeeds[assetKey_].assetAddr != address(0),
            "PriceManager: The oracle for assets does not exists."
        );

        IFxPriceFeed priceFeed_ = priceFeeds[assetKey_].priceFeed;

        if (address(priceFeed_) == address(0)) {
            return (10 ** PRICE_DECIMALS, PRICE_DECIMALS);
        }

        return (priceFeed_.exchangeRate(), uint8(priceFeed_.decimalPlaces()));
    }
}
