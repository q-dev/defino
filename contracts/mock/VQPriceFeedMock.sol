// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.17;

import "../interfaces/q-system-contracts/IFxPriceFeed.sol";

contract VQPriceFeedMock is IFxPriceFeed {
    string public override pair;

    address public override baseTokenAddr;

    uint256 public override decimalPlaces;

    uint256 public override updateTime;

    uint256 public override exchangeRate;

    constructor(address baseTokenAddress_) {
        pair = "VQ_QUSD";
        decimalPlaces = 18;
        updateTime = block.timestamp;
        exchangeRate = 333333333333333333;

        baseTokenAddr = baseTokenAddress_;
    }
}
