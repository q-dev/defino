// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.17;

import "../interfaces/q-system-contracts/IFxPriceFeed.sol";

contract FxPriceFeedMock is IMutableFxPriceFeed {
    string public override pair;

    address public override baseTokenAddr;

    uint256 public override decimalPlaces;

    uint256 public override updateTime;

    uint256 public override exchangeRate;

    constructor(uint256 exchangeRate_, uint256 decimalPlaces_) {
        exchangeRate = exchangeRate_;
        decimalPlaces = decimalPlaces_;
    }

    function setExchangeRate(uint256 exchangeRate_) external returns (bool) {
        return setExchangeRate(exchangeRate_, block.timestamp);
    }

    function setExchangeRate(
        uint256 exchangeRate_,
        uint256 updateTime_
    ) public override returns (bool) {
        exchangeRate = exchangeRate_;
        updateTime = updateTime_;

        return true;
    }
}
