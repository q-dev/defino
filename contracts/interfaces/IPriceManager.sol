// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.17;

import "./q-system-contracts/IFxPriceFeed.sol";

/**
 * This contract is responsible for obtaining asset prices from trusted oracles.
 * The contract code provides for a main oracle and a backup oracle, as well as the ability to switch all price fetches to a backup oracle
 */
interface IPriceManager {
    /// @notice The structure that contains the oracle address token for this token
    /// @param assetAddr address of the asset for which the oracles will be saved
    /// @param qPriceFeed address of the price feed for the passed asset
    struct PriceFeed {
        address assetAddr;
        IFxPriceFeed priceFeed;
    }

    /// @notice This event is emitted when a new oracle is added
    /// @param assetKey_ the pool key for which oracles are added
    /// @param qPriceFeed_ the address of the  price feed for the passed asset
    event OracleAdded(bytes32 assetKey_, address qPriceFeed_);

    /// @notice The function you need to add oracles for assets
    /// @dev Only SystemPoolsRegistry contract can call this function
    /// @param assetKey_ the pool key for which oracles are added
    /// @param assetAddr_ address of the asset for which the oracles will be added
    /// @param qPriceFeed_ the address of the  price feed for the passed asset
    function addOracle(bytes32 assetKey_, address assetAddr_, address qPriceFeed_) external;

    /// @notice The function that returns the price for the asset for which oracles are saved
    /// @param assetKey_ the key of the pool, for the asset for which the price will be obtained
    /// @return answer - the resulting token price
    /// @return decimals - resulting token price decimals
    function getPrice(bytes32 assetKey_) external view returns (uint256, uint8);
}
